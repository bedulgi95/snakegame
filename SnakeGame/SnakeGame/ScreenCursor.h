#pragma once

#include "GameMap.h"

#include <Windows.h>

enum KeyInput
{
    UP = 0x48, DOWN = 0x50, LEFT = 0x4B, RIGHT = 0x4D
};

enum WallPos
{
    UPSPACE = 1, LEFTSPACE = 1
};

enum MoveState
{
    BACK = -1, STOP = 0, FORWARD = 1
};

static GameMap mapInfo;

static MoveState ProgressX = STOP;
static MoveState ProgressY = STOP;

static int map_height = mapInfo.GetMap_Height();
static int map_width = mapInfo.GetMap_Width() * 2;

static int posX = 1 * 2;
static int posY = 1;

void SetPosXY(int x, int y);
void SetMapSize(int h, int w);

void GoUp();
void GoDown();
void GoLeft();
void GoRight();

bool IsCollide();

void ProgressMove();
void KeyInput();