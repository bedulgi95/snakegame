#pragma once

#include <string>

enum TileTypeDiv
{
    APPLE, FLOOR, WALL
};

class TileType
{
public:
    TileType();

    void SetPos(int x, int y);
    void SetTileType(TileTypeDiv type);

    int GetPosX() { return posX; }
    int GetPosY() { return posY; }
    TileTypeDiv GetTileType() { return tileType; }

    std::string ShowThisTile() { return tileImg[tileType]; }

    ~TileType();

private:
    int posX, posY;

    TileTypeDiv tileType;

    std::string tileImg[3] = { "��", "��", "��" };
};