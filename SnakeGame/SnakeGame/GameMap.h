#pragma once

#include "TileType.h"
#include "RandomNumGen.h"
#include "ScreenCursor.h"

class GameMap
{
public:
    GameMap();
    GameMap(int h, int w);

    void SetMap();
    void SetMap(int h, int w);
    void InitMap();

    void SetSnakeStart();
    void SnakeDirection();

    void SetApple();

    void ShowMap();

    int GetMap_Height() { return map_height; }
    int GetMap_Width() { return map_width; }

    ~GameMap();

private:
    static int map_height;
    static int map_width;

    TileType ** gameMap;
    TileType ** snake;
    RandNumGen * randX, * randY;
    RandNumGen * snakeRandX, *snakeRandY;
};