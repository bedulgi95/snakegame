#include "TileType.h"

TileType::TileType() : posX(0), posY(0), tileType(FLOOR)
{
}

void TileType::SetPos(int x, int y)
{
    posX = x; posY = y;
}

void TileType::SetTileType(TileTypeDiv type)
{
    tileType = type;
}

TileType::~TileType()
{
}