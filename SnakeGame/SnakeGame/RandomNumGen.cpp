#include "RandomNumGen.h"

RandNumGen::RandNumGen() : MIN(10), MAX(10)
{
}

RandNumGen::RandNumGen(int min, int max) : MIN(min), MAX(max)
{
}

void RandNumGen::RandomSeedGenerate()
{
    m_randomGen.seed(m_randomDevice());
    seedGenerateState = true;
}

void RandNumGen::RandomNumRange()
{
    if (seedGenerateState == false) RandomSeedGenerate();

    decltype(m_uniform.param()) range(MIN, MAX);
    m_uniform.param(range);
}

RandNumGen::~RandNumGen()
{
}
