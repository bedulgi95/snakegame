#include "ScreenCursor.h"

#include <conio.h>

void SetPosXY(int x, int y)
{
    COORD cursorPos = { x, y };

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursorPos);
}

void SetMapSize(int h, int w)
{
    map_height = h;
    map_width = w * 2;
}

void GoUp()
{
    ProgressX = STOP;
    ProgressY = BACK;
}

void GoDown()
{
    ProgressX = STOP;
    ProgressY = FORWARD;
}

void GoLeft()
{
    ProgressX = BACK;
    ProgressY = STOP;
}

void GoRight()
{
    ProgressX = FORWARD;
    ProgressY = STOP;
}

bool IsCollide()
{
    if (posX >= UPSPACE && posX <= map_width && posY >= LEFTSPACE && posY <= map_height)
    {
        return false;
    }

    else return true;
}

void ProgressMove()
{
    posX += ProgressX * 2;
    posY += ProgressY;

    SetPosXY(posX, posY);
}

void KeyInput()
{
    if (_kbhit())
    {
        switch (_getch())
        {
        case UP:
            GoUp();
            break;
        case DOWN:
            GoDown();
            break;
        case LEFT:
            GoLeft();
            break;
        case RIGHT:
            GoRight();
            break;
        }
    }
}