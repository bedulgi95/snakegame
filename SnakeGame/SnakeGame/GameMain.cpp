#include "ScreenCursor.h"
#include "GameMap.h"

#include <iostream>

int GameMap::map_height = 20;
int GameMap::map_width = 20;

bool RandNumGen::seedGenerateState = false;

int main(int argc, char *argv[])
{
    GameMap map;

    map.ShowMap();

    do {
        for (int speed = 0; speed < 4; speed++)
        {
            KeyInput();
            Sleep(50);
        }
        KeyInput();
        ProgressMove();
    } while (!IsCollide());

    return 0;
}