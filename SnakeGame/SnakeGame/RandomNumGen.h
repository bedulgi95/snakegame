#pragma once

#include <random>

class RandNumGen
{
public:
    RandNumGen();
    RandNumGen(int min, int max);

    void RandomSeedGenerate();
    void RandomNumRange();
    const int RandomNumGen() { return m_uniform(m_randomGen); }

    ~RandNumGen(); 

private:
    const int MIN;
    const int MAX;

    static bool seedGenerateState;

    std::random_device m_randomDevice;
    std::default_random_engine m_randomGen;
    std::uniform_int_distribution<int> m_uniform;
};

