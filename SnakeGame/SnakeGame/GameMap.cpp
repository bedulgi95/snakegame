#include "GameMap.h"

#include <iostream>

GameMap::GameMap()
{
    SetMap();
}

GameMap::GameMap(int h, int w)
{
    SetMap(h, w);
}

void GameMap::SetMap()
{
    randX = new RandNumGen(1, map_width);
    randY = new RandNumGen(1, map_height);

    gameMap = new TileType *[map_height + 2];
    for (int i = 0; i < map_height + 2; i++) {
        gameMap[i] = new TileType[map_width + 2];
    }

    snake = new TileType *[map_height + 2];
    for (int i = 0; i < map_height + 2; i++) {
        snake[i] = new TileType[map_width + 2];
    }

    InitMap();
}

void GameMap::SetMap(int h, int w)
{
    map_height = h, map_width = w;

    SetMap();
}

void GameMap::InitMap()
{
    for (int i = 0; i < map_width + 2; i++)
    {
        gameMap[0][i].SetTileType(WALL);
    }
    for (int i = 1; i < map_height + 1; i++)
    {
        gameMap[i][0].SetTileType(WALL);
        for (int j = 1; j < map_width + 1; j++)
        {
            gameMap[i][j].SetTileType(FLOOR);
        }
        gameMap[i][map_width + 1].SetTileType(WALL);
    }
    for (int i = 0; i < map_width + 2; i++)
    {
        gameMap[map_height + 1][i].SetTileType(WALL);
    }
}

void GameMap::SetSnakeStart()
{
}

void GameMap::SnakeDirection()
{
}

void GameMap::SetApple()
{
    gameMap[randY->RandomNumGen()][randX->RandomNumGen()].SetTileType(APPLE);
}

void GameMap::ShowMap()
{
    for (int i = 0; i < map_height + 2; i++)
    {
        for (int j = 0; j < map_width + 2; j++)
        {
            std::cout << gameMap[i][j].ShowThisTile();
        }
        std::cout << '\n';
    }
}

GameMap::~GameMap()
{
    for (int i = 0; i < map_height; i++) {
        delete[] gameMap[i];
    }
    delete[] gameMap;

    delete[] snake;

    delete randX;
    delete randY;
}